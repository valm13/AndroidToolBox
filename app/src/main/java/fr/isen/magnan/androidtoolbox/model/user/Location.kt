package fr.isen.magnan.androidtoolbox.model.user

data class Location(var street: Street, var city : String, var state : String, var country : String, var postcode : String)