package fr.isen.magnan.androidtoolbox.model.user

data class Picture(var large : String, var medium : String, var thumbnail : String)