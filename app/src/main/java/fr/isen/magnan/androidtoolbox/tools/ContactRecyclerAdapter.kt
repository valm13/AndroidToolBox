package fr.isen.magnan.androidtoolbox.tools

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.isen.magnan.androidtoolbox.R
import fr.isen.magnan.androidtoolbox.model.ContactModel
import kotlinx.android.synthetic.main.contacts_list_item.view.*

class ContactRecyclerAdapter(private val items: ArrayList<ContactModel>, private val context: Context) : RecyclerView.Adapter<ContactRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.contacts_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    // Bind each contact in the Arraylist to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = items[position].name
        holder.tvNumber.text = items[position].number
    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val tvName = view.nameContact
        val tvNumber = view.numberContact
    }
}