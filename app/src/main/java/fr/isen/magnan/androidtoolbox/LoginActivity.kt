package fr.isen.magnan.androidtoolbox

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.isen.magnan.androidtoolbox.miscellaneous.SingleToast
import kotlinx.android.synthetic.main.activity_login.*




class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sharedPreferences = getSharedPreferences("loginPreferences", Context.MODE_PRIVATE)

        if(sharedPreferences.getString("username","") == "admin" && sharedPreferences.getString("password","") == "123")
        {
            val intentHome = Intent(this, HomeActivity::class.java)
            startActivity(intentHome)
            finish()
        }

        login_valider.setOnClickListener {
            if(authUser())
            {
                val intentHome = Intent(this, HomeActivity::class.java)
                startActivity(intentHome)
                finish()
            }
        }

        showHidebutton.setOnClickListener {
            if(showHidebutton.text.toString().equals("Show")){
                password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                showHidebutton.text = "Hide"
            } else{
                password.transformationMethod = PasswordTransformationMethod.getInstance()
                showHidebutton.text = "Show"
            }
        }


    }

    private fun authUser() : Boolean{
        if(username.text.toString() == "admin" && password.text.toString() == "123")
        {
            val sharedPreferences = getSharedPreferences("loginPreferences", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("username",username.text.toString())
            editor.putString("password",password.text.toString())
            editor.apply()
            SingleToast.show(this, "Connection réussie", Toast.LENGTH_SHORT)
            return true
        }

        SingleToast.show(this, "Nom d'utilisateur ou mot de passe incorrect", Toast.LENGTH_SHORT)
        return false
    }
}
