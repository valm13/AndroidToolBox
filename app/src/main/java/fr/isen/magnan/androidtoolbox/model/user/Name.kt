package fr.isen.magnan.androidtoolbox.model.user

data class Name(var title : String, var first : String, var last : String)