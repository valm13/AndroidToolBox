package fr.isen.magnan.androidtoolbox

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import fr.isen.magnan.androidtoolbox.model.ContactModel
import fr.isen.magnan.androidtoolbox.tools.ContactRecyclerAdapter
import kotlinx.android.synthetic.main.activity_permissions.*


class PermissionsActivity : AppCompatActivity(),LocationListener {

        companion object {
        const val CHOOSER_REQUEST_CODE = 666
        const val REQUEST_CODE = 1
        val ALL_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS)
        val LOCATION_PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION)
        const val UPDATE_TIME_LOC = 1000L
        const val DUREE_TOAST = Toast.LENGTH_LONG
        private var locationManager: LocationManager? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)

        askAllRequiredPermissions()
        camera.setOnClickListener {
            choice()
        }
    }

    private fun choice() {

        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        val chooserIntent = Intent(Intent.ACTION_CHOOSER)
        chooserIntent.putExtra(Intent.EXTRA_INTENT, galleryIntent)
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Votre choix")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(cameraIntent))

        startActivityForResult(chooserIntent, CHOOSER_REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOOSER_REQUEST_CODE) {
                if (data?.extras?.get("data") == null)
                    camera.setImageURI(data?.data)
                else
                    camera.setImageBitmap(data.getParcelableExtra("data"))
            }
        }

    }

    private fun askAllRequiredPermissions() {
        if(Build.VERSION.SDK_INT >= 23)
            ActivityCompat.requestPermissions(this, ALL_PERMISSIONS,REQUEST_CODE)
        else{
            getContacts()
            getLoc()
        }
    }

    private fun checkLocPermissions() : Boolean{
        LOCATION_PERMISSIONS.forEach {
            if(ActivityCompat.checkSelfPermission(this,it) == PackageManager.PERMISSION_DENIED)
                return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE || (Build.VERSION.SDK_INT < 23))
        {
            getLoc()
            getContacts()
        }
    }

    private fun getLoc(){

        if (((ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) &&
            (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED))|| (Build.VERSION.SDK_INT < 23)){

            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

            locationManager?.let {

                it.requestLocationUpdates(
                    if(Build.VERSION.SDK_INT < 23)LocationManager.GPS_PROVIDER else LocationManager.NETWORK_PROVIDER,
                    UPDATE_TIME_LOC,
                    1f,
                    this
                )
                val location = it.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if(location != null){
                    latitudeval.text = location.latitude.toBigDecimal().toPlainString()
                    longitudeval.text = location.longitude.toBigDecimal().toPlainString()
                }

            }
        }
        else
            Toast.makeText(this,"Permission manquante : GPS",DUREE_TOAST).show()
    }

    private fun getContacts() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED )|| (Build.VERSION.SDK_INT < 23)) {
            // Récupération des contacts
            val contacts = ArrayList<ContactModel>()
            val cursor  = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC")
            cursor?.let{
                if(cursor.count > 0)
                {
                    while(cursor.moveToNext()){
                        val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))

                        val numberList = getNumbers(id)
                        var number = "no info"
                        if(numberList.size > 0)
                            number = numberList[0]

                        contacts.add(ContactModel(name,number))
                    }
                }
                cursor.close()
            }
            recycler.layoutManager = LinearLayoutManager(this)
            val dividerItemDecoration = DividerItemDecoration(this,DividerItemDecoration.VERTICAL)
            recycler.addItemDecoration(dividerItemDecoration)
            recycler.adapter = ContactRecyclerAdapter(contacts,applicationContext)
        }
        else
            Toast.makeText(this,"Permission manquante : CONTACTS",DUREE_TOAST).show()
    }

    private fun getNumbers(id: String): ArrayList<String> {
        val cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + id, null, null)
        val numberList = ArrayList<String>()
        cursor?.let {
            while (cursor.moveToNext())
                numberList.add(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)))
            cursor.close()
        }
        return numberList
    }

    override fun onLocationChanged(location: Location?) {
        if(location != null){
            latitudeval.text = location.latitude.toBigDecimal().toPlainString()
            longitudeval.text = location.longitude.toBigDecimal().toPlainString()
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    @SuppressLint("MissingPermission")
    override fun onProviderEnabled(p0: String?) {
        if(Build.VERSION.SDK_INT >= 23)
        {
            if(checkLocPermissions()) {
                locationManager.let {
                    it?.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        UPDATE_TIME_LOC,
                        1f,
                        this
                    )
                }
            }
        }
        else
        {
            locationManager.let {
                it?.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    UPDATE_TIME_LOC,
                    1f,
                    this
                )
            }
        }


    }

    override fun onProviderDisabled(p0: String?) {
        latitudeval.text = "GPS désactivé"
        longitudeval.text = "GPS désactivé"
    }

}