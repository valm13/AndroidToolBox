package fr.isen.magnan.androidtoolbox

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import fr.isen.magnan.androidtoolbox.model.user.UserModel
import fr.isen.magnan.androidtoolbox.tools.UserRecyclerAdapter
import kotlinx.android.synthetic.main.activity_web_services.*
import org.json.JSONObject

class WebServicesActivity : AppCompatActivity() {


    companion object{
        const val url = "https://randomuser.me/api/?inc=name,email,phone,picture,location&results=20"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_services)

        val users : ArrayList<UserModel> = ArrayList()

        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recyclerWebServices.layoutManager = LinearLayoutManager(this)
        recyclerWebServices.addItemDecoration(dividerItemDecoration)
        recyclerWebServices.adapter = UserRecyclerAdapter(users,applicationContext)
        requestUsers(users)
        addRecyclerScrollListener(users)
    }
    private fun addRecyclerScrollListener(users : ArrayList<UserModel>)
    {
        recyclerWebServices.addOnScrollListener(object :  RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                /* Si on scroll vers le bas mais qu'on y est déjà, on rappelle requestUsers */
                if(!recyclerView.canScrollVertically(1))
                    requestUsers(users)
            }
        })
    }
    private fun requestUsers(users : ArrayList<UserModel>)
    {
        val queue = Volley.newRequestQueue(this)

        // Request a string response from the provided URL.
        val request = JsonObjectRequest(
            Request.Method.GET, url,null,
            Response.Listener<JSONObject> {

                val jsonArray = it.getJSONArray("results")

                for (i in 0 until jsonArray.length()) {
                    val user = Gson().fromJson(jsonArray[i].toString(), UserModel::class.java)
                    users.add(user)
                }

                    recyclerWebServices.adapter?.notifyDataSetChanged()
            },
            Response.ErrorListener {
                //Error
                Log.d("error",it.toString())
                 })
        // Add the request to the RequestQueue.
        queue.add(request)
    }
}
