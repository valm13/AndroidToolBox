package fr.isen.magnan.androidtoolbox.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import fr.isen.magnan.androidtoolbox.R
import kotlinx.android.synthetic.main.fragment_lifecycle.*

class LifeCycleFragment : Fragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_lifecycle, container, false)
        }

        override fun onStart() {
            super.onStart()
            notify("Fg")
        }

        override fun onResume() {
            super.onResume()
            notify("Fg")
        }

        override fun onPause() {
            super.onPause()
            notify("Bg")
        }

        override fun onStop() {
            super.onStop()
            notify("Bg")
        }

        override fun onDestroy() {
            super.onDestroy()
            notify("Destroyed")
        }

        private fun notify(state : String){
            when (state) {
                "Fg" -> fragment_textview.text = getString(R.string.fragment_fg)
                "Bg" -> Log.d("Tag",getString(R.string.fragment_bg))
                "Destroyed" -> Toast.makeText(context,getString(R.string.fragment_destroy), Toast.LENGTH_SHORT).show()
            }
        }
}