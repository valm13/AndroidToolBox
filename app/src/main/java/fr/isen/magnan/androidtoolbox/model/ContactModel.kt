package fr.isen.magnan.androidtoolbox.model


data class ContactModel(var name: String = "no info", var number: String? = "no info")