package fr.isen.magnan.androidtoolbox

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setbuttonListeners()

    }

    private fun setbuttonListeners()
    {
        lifecycle_button.setOnClickListener {
            startActivity(Intent(this, LifeCycleActivity::class.java))
        }

        save_button.setOnClickListener {
            startActivity(Intent(this, FormActivity::class.java))
        }

        permissions_button.setOnClickListener {
            startActivity(Intent(this, PermissionsActivity::class.java))
        }

        webservices_button.setOnClickListener {
            val intent = Intent(this,WebServicesActivity::class.java)
            startActivity(intent)
        }

        logout.setOnClickListener{
            val sharedPreferences = getSharedPreferences("loginPreferences", Context.MODE_PRIVATE)
            sharedPreferences.edit().clear().apply()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}
