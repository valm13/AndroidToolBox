package fr.isen.magnan.androidtoolbox.tools

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.isen.magnan.androidtoolbox.R
import fr.isen.magnan.androidtoolbox.model.user.UserModel
import kotlinx.android.synthetic.main.user_list_item.view.*

class UserRecyclerAdapter(private val items: ArrayList<UserModel>, private val context: Context) : RecyclerView.Adapter<UserRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    // Bind each contact in the Arraylist to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.lastName.text = items[position].name.last
        holder.firstName.text = items[position].name.first
        holder.numberStreet.text = items[position].location.street.number
        holder.nameStreet.text = items[position].location.street.name
        holder.city.text = items[position].location.city
        holder.email.text = items[position].email
        Picasso.get().load(items[position].picture.large).into(holder.img)

    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val lastName = view.lastnameUser
        val firstName = view.firstnameUser
        val numberStreet = view.numberStreetLocationUser
        val nameStreet = view.nameStreetLocationUser
        val city = view.cityLocationUser
        val img = view.imgUser
        val email = view.emailUser
    }
}
