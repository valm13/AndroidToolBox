package fr.isen.magnan.androidtoolbox.model

class Post {

    var postNom :String? = null
    var postPrenom :String? = null
    var postDate : String? = null

    constructor(PostNom : String, PostPrenom : String, PostDate : String) : super() {
        this.postNom = PostNom
        this.postPrenom = PostPrenom
        this.postDate = PostDate
    }

    override fun toString(): String {
        return "Post(postNom=$postNom, postPrenom=$postPrenom, postDate=$postDate)"
    }
}