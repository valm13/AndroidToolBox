package fr.isen.magnan.androidtoolbox

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import fr.isen.magnan.androidtoolbox.miscellaneous.SingleToast
import fr.isen.magnan.androidtoolbox.model.Post
import kotlinx.android.synthetic.main.activity_form.*
import java.io.File
import java.util.*

class FormActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        dateofbirth.setOnClickListener {
            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    val normalMonth = mMonth+1
                    val date = "$mDay/$normalMonth/$mYear"
                    dateofbirth.setText(date)
                },
                year,
                month,
                day
            )
            dpd.datePicker.minDate = System.currentTimeMillis() - 2522880000000
            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

        saveform.setOnClickListener {
            // Si tout est remplis on enregistre les infos dans un fichier JSON
            if(nom.text.toString().length >= 1 && prenom.text.toString().length >= 1 && dateofbirth.text.toString() != "Date de naissance")
            {
                val post = Post(prenom.text.toString(),nom.text.toString(),dateofbirth.text.toString())
                val gson = Gson()
                val jsonString:String = gson.toJson(post)

                val file= File(cacheDir.absolutePath + "/users.json")
                file.writeText(jsonString)
                SingleToast.show(this,"Sauvegarde effectuée", Toast.LENGTH_SHORT)
            }
            else
                SingleToast.show(this,"Veuillez remplir tout les champs", Toast.LENGTH_SHORT)
        }

        displayForm.setOnClickListener {
            val file= File(cacheDir.absolutePath + "/users.json")
            if(file.exists())
            {
                val data = Gson().fromJson(file.bufferedReader().use { it.readText() }, Post::class.java)

                data.postDate?.let {
                    val d = getDateFromString(it)

                    val alert = AlertDialog.Builder(this)

                    alert.setMessage("Vos informations :\n"+
                            "Nom : "+data.postNom+"\n"+
                            "Prenom : "+data.postPrenom+"\n"+
                            "Date de naissance : "+data.postDate+"\n"+
                            "Age : "+ getAge(d))
                    alert.create().show()
                }

            }
            else
                SingleToast.show(this,"Aucune données enregistrées", Toast.LENGTH_SHORT)
        }

    }

    private fun getDateFromString(s: String) : Calendar{
        var step = 0
        var day = 0
        var month = 0
        var year = 0
        s.forEach {
            if(it == '/')
                step++
            else
            {
                val actualNumber = (it.toInt() - '0'.toInt())
                when (step){
                    0 -> day = day * 10 + actualNumber
                    1 -> month = month * 10 + actualNumber
                    2 -> year = year * 10 + actualNumber
                }
            }
        }
        val birthdate :Calendar = Calendar.getInstance()
        birthdate.set(year,month,day)
        return birthdate
    }

    private fun getAge(date: Calendar) : Int{
        val today = Calendar.getInstance()

        var age :Int = today.get(Calendar.YEAR) - date.get(Calendar.YEAR)

        val actualDayOfYear : Int = today.get(Calendar.DAY_OF_YEAR)
        val dateDayOfYear : Int = date.get(Calendar.DAY_OF_YEAR)

        // Si l'anniversaire est plus tard dans l'année
        if(actualDayOfYear < dateDayOfYear)
            age--

        if(age == -1)
            return 0

        return age
    }

}
