package fr.isen.magnan.androidtoolbox

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_life_cycle.*

class LifeCycleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_life_cycle)

        notify("Fg")
    }

    override fun onStart() {
        super.onStart()
        notify("Fg")
    }

    override fun onResume() {
        super.onResume()
        notify("Fg")
    }

    override fun onPause() {
        super.onPause()
        notify("Bg")
    }

    override fun onStop() {
        super.onStop()
        notify("Bg")
    }

    override fun onDestroy() {
        super.onDestroy()
        notify("Destroyed")
    }

    private fun notify(state : String){
        when (state) {
            "Fg" -> actualcycle_textview.text = getString(R.string.activity_fg)
            "Bg" -> Log.d("Tag",getString(R.string.activity_bg))
            "Destroyed" -> Toast.makeText(this,getString(R.string.activity_destroy), Toast.LENGTH_SHORT).show()
        }
    }
}
