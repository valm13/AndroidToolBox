package fr.isen.magnan.androidtoolbox.model.user

data class UserModel(var name : Name, var location : Location, var email : String, var phone : String, var picture : Picture)