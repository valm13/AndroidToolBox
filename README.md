# Projet Android 2020


**Partie 1 : Création et configuration du projet**
*  Source dans le package fr.isen.magnan.androidtoolbox
*  Niveau d'API minimum : 19 pour être fonctionnel chez 95% des utilisateurs
*  Utilisation de l'émulateur Android, il est également possible de brancher son téléphone en USB avec le débogage usb activé et en mode transfert de fichier

**Partie 2 : Manipulation de l'interface utilisateur**
*  Création de la page de login
*  Création de la page home

**Partie 3 : Cycle de vie et fragment**
*  Mise en évidence du cycle d'une activité
*  Utilisation des fragments

**Partie 4 : Sauvegarder les informations au sein d'une application**
*  SharedPreferences
*  Fichier JSON

**Partie 5 : Accès fonctionnalités du smartphone et permissions**
*  Remplacement d'une image en forme de caméra par une image prise par la caméra ou dans une galerie photo
*  Listing des contacts du téléphone avec un RecyclerView
*  Affichage des coordonnées GPS avec une update régulière de celles-ci

**Partie 6 : Utilisation des WebServices et de librairie**
*  Listing d'utilisateur récupéré grâce à l'API : https://randomuser.me et la librairie Volley pour faire les requêtes
*  Affichage grâce à un RecyclerView
*  Utilisation de la librairie Picasso pour afficher les images des utilisateurs

**Informations additionnelles**
*  APK générée et signée dont le chemin est le suivant : "app/release/app-release.apk"
*  Projet réalisé dans le cadre du cours de développement mobile au 2ème semestre de M1 à l'*ISEN TOULON*
*  Language de programmation utilisé : Kotlin et XML